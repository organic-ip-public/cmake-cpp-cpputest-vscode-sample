// To add a new test group,
// copy this file to new name. Keep xxxxTestGroup.cpp
// In new file, rename SampleTestGroup to xxxxTestGroup
// Add reference to this file in AllTests.cpp file
//      IMPORT_TEST_GROUP(xxxxxTestGroup);

// Add reference to your new .cpp file in tests/CMakeLists.txt
//  add_executable(RunAllTests
//    AllTests.cpp
//    #### Add all your xxxxxTestGroup.cpp here
//    SerieDetectTestGroup.cpp
//                             # <- Add your line here
//    )

#include "CppUTest/TestHarness.h"

#include "../src/lib/serie_detectclass.h"

#ifdef __cplusplus
extern "C"
{
#endif

#ifdef __cplusplus
}
#endif

static serieDetect_c serieDetect;
unsigned int lowmark;
unsigned int hightmark;

TEST_GROUP(SerieDetectTestGroup){

    void setup(){
        // Init stuff
        serieDetect.cleanup();
lowmark = 2;
hightmark = 4;
serieDetect.setMark(lowmark, hightmark);
}

void teardown()
{
}
void CHECK_up_n_down(unsigned int inlowmark, unsigned int inhightmark)
{
    serieDetect.cleanup();
    serieDetect.setMark(inlowmark, inhightmark);
    inlowmark = serieDetect.getLowMark();
    inhightmark = serieDetect.getHiMark();

    char txt[4096];
    txt[4095] = '\0';

    for (unsigned int i = 1; i < inhightmark; i++)
    {
        bool resuFalse = serieDetect.addnCheck4True(true);
        snprintf(txt, 4094, "i=%i climbing, result should be false below hightmark(%i)", i, inhightmark);
        CHECK_FALSE_TEXT(resuFalse, txt);
    }
    for (unsigned int i = inhightmark; i <= serieDetect.getserieSize(); i++)
    {
        bool resuTrue = serieDetect.addnCheck4True(true);
        snprintf(txt, 4094, "i=%i climbing, result should be True above hightMark(%i)", i, inhightmark);
        CHECK_TRUE_TEXT(resuTrue, txt);
    }

    for (unsigned int i = serieDetect.getserieSize(); i > (inlowmark + 1); i--)
    {
        bool resuTrue = serieDetect.addnCheck4True(false);
        snprintf(txt, 4094, "i=%i Going down. result should be True above lowmark(%i)", i, inlowmark);
        CHECK_TRUE_TEXT(resuTrue, txt);
    }
    for (int i = (int) inlowmark; i >= 0; i--)
    {
        bool resuFalse = serieDetect.addnCheck4True(false);
        snprintf(txt, 4094, "i=%i going down. Result should be falst below  lowmark(%i)", i, inlowmark);
        CHECK_FALSE_TEXT(resuFalse, txt);
    }
}
}
;

TEST(SerieDetectTestGroup, Zero_Max)
{
    CHECK_up_n_down(0, serieDetect.getserieSize());
}

TEST(SerieDetectTestGroup, One_MinusOne)
{
    CHECK_up_n_down(1, serieDetect.getserieSize() - 1);
}
TEST(SerieDetectTestGroup, Two_MinusTwo)
{
    CHECK_up_n_down(2, serieDetect.getserieSize() - 2);
}


TEST(SerieDetectTestGroup, OverFLowHiMark)
{
    CHECK_up_n_down(1, 100);
}

TEST(SerieDetectTestGroup, OverFLowLowHiMark)
{
    CHECK_up_n_down(101, 100);
}

TEST(SerieDetectTestGroup, SAMPLE){
    CHECK_TRUE_TEXT(false,"Ce test plante depuis le fichier tests/SerieDetectTestGroup.cpp. Read READMEfile :-)");
}
