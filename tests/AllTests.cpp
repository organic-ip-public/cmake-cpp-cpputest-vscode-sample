//From :  http://cpputest.github.io/

#include "CppUTest/CommandLineTestRunner.h"
#include <stdio.h>
#include <errno.h>

// IMPORT_TEST_GROUP(SampleTestGroup);
// INFORMATION : 
// Les tests sont joués du dernier au premiers. Il faut ajouter les nouveaux tests en haut

// Your new tests above here
IMPORT_TEST_GROUP(SerieDetectTestGroup);
//IMPORT_TEST_GROUP(SampleTestGroup)


// Static for NULL init all all field
const int av2Size = 0;
// Will force all this as firsts default arguments
// Because debug and cmake debug doesnot start from here, force -c -vdirectly in main.c

static const char *avDefault[] = {
    "", // Will be replace by original arv[0]
    "-c",
    //"-v",
    NULL};

const int av2PhysSize = 256;
static const char *av2[av2PhysSize] = {};

int main(int ac, char *av[])
{
   // Append arguments from av to av2.
   int avDefaultSize = 0;
   while (avDefault[avDefaultSize++] != NULL)
   {
      ;
   }
   int av2Size = ac + avDefaultSize + 1;
   if (av2Size >= av2PhysSize)
   {
      perror("pre-Reserved size is not anought for processing arguments. Incrising av2PhysSize value. Exiting");
      exit(1);
   }

   int av2i = 0;
   int avi = 0;
   av2[av2i++] = av[avi++];
   while (avDefault[av2i] != NULL)
   {
      av2[av2i] = avDefault[av2i];
      av2i++;
   }
   do
   {
      av2[av2i] = av[avi];
   } while (av2i++ < (av2Size - 1) && av[avi++] != NULL);
   av2[av2Size - 1] = NULL;
   return CommandLineTestRunner::RunAllTests(av2i - 1, av2);
}

// https://cpputest.github.io/manual.html#assertions

// The failure of one of these macros causes the current test to immediately exit:

//     CHECK(boolean condition) - checks any boolean result.
//     CHECK_TEXT(boolean condition, text) - checks any boolean result and prints text on failure.
//     CHECK_FALSE(condition) - checks any boolean result
//     CHECK_EQUAL(expected, actual) - checks for equality between entities using ==. So if you have a class that supports operator==() you can use this macro to compare two instances. You will also need to add a StringFrom() function like those found in SimpleString. This is for printing the objects when the check failed.
//     CHECK_COMPARE(first, relop, second) - checks thats a relational operator holds between two entities. On failure, prints what both operands evaluate to.
//     CHECK_THROWS(expected_exception, expression) - checks if expression throws expected_exception (e.g. std::exception). CHECK_THROWS is only available if CppUTest is built with the Standard C++ Library (default).
//     STRCMP_EQUAL(expected, actual) - checks const char* strings for equality using strcmp().
//     STRNCMP_EQUAL(expected, actual, length) - checks const char* strings for equality using strncmp().
//     STRCMP_NOCASE_EQUAL(expected, actual) - checks const char* strings for equality, not considering case.
//     STRCMP_CONTAINS(expected, actual) - checks whether const char* actual contains const char* expected.
//     LONGS_EQUAL(expected, actual) - compares two numbers.
//     UNSIGNED_LONGS_EQUAL(expected, actual) - compares two positive numbers.
//     BYTES_EQUAL(expected, actual) - compares two numbers, eight bits wide.
//     POINTERS_EQUAL(expected, actual) - compares two pointers.
//     DOUBLES_EQUAL(expected, actual, tolerance) - compares two floating point numbers within some tolerance
//     FUNCTIONPOINTERS_EQUAL(expected, actual) - compares two void (*)() function pointers
//     MEMCMP_EQUAL(expected, actual, size) - compares two areas of memory
//     BITS_EQUAL(expected, actual, mask) - compares expected to actual bit by bit, applying mask
//     FAIL(text) - always fails
