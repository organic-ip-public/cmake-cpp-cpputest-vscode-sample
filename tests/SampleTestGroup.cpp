// To add a new test group, 
// copy this file to new name. Keep xxxxTestGroup.cpp
// In new file, rename SampleTestGroup to xxxxTestGroup
// Add reference to this file in AllTests.cpp file
//      IMPORT_TEST_GROUP(xxxxxTestGroup);

// Add reference to your new .cpp file in tests/CMakeLists.txt
//  add_executable(RunAllTests
//    AllTests.cpp
//    #### Add all your xxxxxTestGroup.cpp here
//    BlinkersTestGroup.cpp  
//                             # <- Add your line here
//    )

#include "CppUTest/TestHarness.h"

#ifdef __cplusplus
extern "C"
{
#include "../pran-visionplex/MLT_EC_once.h"
}
#endif

TEST_GROUP(SampleTestGroup){
    void setup(){
        // Init stuff
    }

    void teardown(){
        // Un-init stuff
    }};

TEST(SampleTestGroup, FirstTest)
{
   FAIL("Ceci est un test qui fait simplement un FAIL");
}
