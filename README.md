# project informations

 This projet is a ready to use  skeleton project with : 
   * c & c++ , 
   * Cmake tools, 
   * cpputest unit tests, 
   * vscode configuration

It is tested on linux / macos extension / (and Windows with cygwin - 60% done only on windows ) 
# Points clefs et installation environnement de dev

## Sur Windows

  * Besoin de putty , rajouter git for windows (qui inclus ssh et ssh-keygen...), 
  * Generation de clefs public/privé
     * ''ssh-keygen.exe -t ed25519''  et pas de mot de passe
  * Enregistrement de la clef public dans le gitlab du projet
  * Si besoin, rajout de l'entrée : c:\Windows\System32\Drivers\etc\hosts
    * 192.168.1.123   lamachine_gitlab_locale
  * option : Ajout de cygwin : https://cygwin.com/install.html
    * base + git + gcc + cmake

## sur Mac

  * Installation de vscode
  * Enregistrement du mot de passe ssh dans le keychain (ne pas utiliser ssh-add de quelqu'un d'autre qu'Apple) : /usr/bin/ssh-add -K .ssh/id_ed25519
  * installer un bash plus récent : brew install bash
  * option : pour avoir un bash plus récent dans le terminal, dans .zshrc, ajouter :
    export PATH="/opt/homebrew/Cellar/bash/5.1.8:$PATH"
  * pour avoir la bonne version de bash dans VS Code :
    * dans .vscode/settings.json ajouter : "terminal.integrated.env.osx": {"PATH":"/opt/homebrew/Cellar/bash/5.1.8:$PATH"}
    * ajouter une virgule à la ligne précédente
  * **ATTENTION** : Il faut que le système de fichier soit case-sensitive sinon ca ne fonctionne pas.

# Setup your developpement env

  * [download and install vscode](https://code.visualstudio.com/Download)
  * add extention : View / Extension : instlal folowing extension:
    * optional : Remote - SSH
    * C/C++ (from microsoft)
    * C/C++ Snippets
    * CMake   [cmake edition]
    * CMake Tools  [build dependencies & compilation ]  => set telemetry.enableTelemetry to disable
    * Git Graph
    * Git blame
    * gitLens - git supercharged
    * cppUTest test adapter
    * MeldDiff  (use meld for diff) + need to install meld on host with : ```apt install meld``` 
    * live share Coding , 
       * you need a microsoft account or github account 
       * Live Share
       * Live Share Audio
       * Live Share Extension Pack

    * disable telemetry : File  / Preference / Settings : telemetry.enableTelemetry  +  Disable

# travailler sur une machine distante

  * Copier la clef ssh locale sur la VM : ssh-copy-id user@hostname
  * la passphrase ssh ne sera pas retenue par vs code ; dans ~/.ssh/config, il faut ajouter :
   `Host yourhost.lan
     ForwardAgent yes`
  * cloner le repository git  : git clone ssh://// => Il faut le faire depuis le terminal pour pouvoir saisir le mot de passe de la clef ssh
  * installer vscode en local (mêmes extension qu'en local)

  * Desactiver la telemetry : File  / Preference / Settings : telemetry.enableTelemetry puis Disable
       
  * Si on veux coder à distance (avec vscode installé sur la machine distante) 
    * palette vscode : remote-ssh : connect to host : IP de la machine distante (la machine distance)
  
  * ouvrir le dossier distant (le repo git qu'on a cloné)   

# outils pour compilation
  * **cmake** : Au  moins la version > 3.10  
    * linux : apt-get install cmake gcc gdb build-essential
    * macos : brew cmake
  * **g++** : Il faut gcc gdb build-essential
    * Try your install with :  ```g++ --version``` 

# editeur & git
  * vscode
    * module : 
      * git history, 
      * C/C++ de microsoft
      * Cmake Tools de Microsoft 

  * windows : sourcetree : checkout de git@xxx:group/project.git
  * macos : gitahead / ...
    * raccourci clavier pour cmake: Run without Debug a vérifier/modifier pour Alt-Ctrl-D si besoin 

## Travail sur le projet 
  * git clone git@githost.xxx:votreprojet/xx.git
  * vscode 
  * Ouvrir la palette  : Ctrl-Shft + P
  * palette : CMake : select a kit (Ca choisi quelle chaine de compilation sera utilisée. Mettre gcc. Ne pas utiliser clang sous linux pcq il y a des incompatibilités avec cpputest). Exp : GCC 9.3.0
    * Après selection, le kit est indiqué dans la bare bleue en bas de l'écran

  * Ctrl-Shift P : Palette pour :
    * Palette : cmake Select Target
      * Choisir RunAllTests (tout en bas de la liste)=> Il s'affiche aussi dans la bare du bas de l'écran
      * A faire une seule fois
    * Palette : cmake configure # Regénération du makefile a partir des fichiers CMakeList.txt
      * A faire dès qu'il y a des nouveaux .cpp/.h/...
    * Palette : cmake build
      * A faire dès qu'on veux compiler le projet (modif de .cpp/.h)

# Informations
## Le moteur de test CCPUTESt
  * Ref : [https://cpputest.github.io/](https://cpputest.github.io/)
    * Il est auto chargé via cmake donc il n'est pas nécessaire de l'installer dans le système. 
    * pour reférence sur macos : brew install cpputest (non nécessaire) 

## Rajout d'un test pour le dev (fichiers tests/xxxTestGroup.cpp)
  * Les informations sont dans le fichier d'example : tests/SampleTestGroup.cpp

 ## Configuration pour la config
   * Configuration des chemins d'include pour cmake
   * C'est dans le fichier tests/CMakeLists.txt


## La référence en CPP : 
  * https://en.cppreference.com/


## License
For open source projects, say how it is licensed.