
#include "serie_detectclass.h"

unsigned int serieDetect_c::lowmark = 2;
unsigned int serieDetect_c::hightmark = 4;

void serieDetect_c::setMark(unsigned int inlowmark, unsigned int inhightmark)
{
    if (inlowmark > inhightmark)
    {
        unsigned int tmp = inlowmark;
        inlowmark = inhightmark;
        inhightmark = tmp;
    }
    if (inhightmark > serieSize)
    {
        inhightmark = serieSize;
    }
    hightmark = inhightmark;
    if (inlowmark > serieSize){
        inlowmark = serieSize;
    }
    lowmark = inlowmark;
}

bool serieDetect_c::_checkSerienUpdateInternalState(void)
{
    unsigned int trueCount = 0;
    for (unsigned int i = 0; i < serieSize; i++)
    {
        if (arr[i] == true)
        {
            trueCount++;
        }
    }
    if (lastconfirmed_val == false && trueCount >= hightmark)
    {
        lastconfirmed_val = true;
        return true;
    }
    if (lastconfirmed_val == true && trueCount <= lowmark)
    {
        lastconfirmed_val = false;
        return false;
    }
    return lastconfirmed_val;
}

bool serieDetect_c::addnCheck4True(bool newvalue)
{
    idx++;
    if (idx >= serieSize)
    {
        idx = 0;
    }
    arr[idx] = newvalue;
    if (lastconfirmed_val != arr[idx])
    {
        return _checkSerienUpdateInternalState();
    }
    return lastconfirmed_val;
}

bool serieDetect_c::get_lastconfirmed(void) const
{
    return lastconfirmed_val;
}

void serieDetect_c::cleanup(void)
{
    for (unsigned int i = 0; i < serieSize; i++)
    {
        arr[i] = false;
    }
    lastconfirmed_val = false;
    idx = 0;
}
