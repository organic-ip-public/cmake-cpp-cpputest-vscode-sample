//blinker_basecpp.h

#ifndef __SERIE_DETECT_CLASS_H_
#define __SERIE_DETECT_CLASS_H_

extern "C"
{
}
#define SERIEDETECT_SIZE (10)
/** Default value are false
     *  Need to have over(>=) hightmark for detection of **real** True
     *  Detect if true is present and over hightmark
     *  After true detection Need to go under(<=) lowmark to re-switch back to false
     * Update and return get_lastconfirmed()
    */
class serieDetect_c
{
public:
    static void setMark(unsigned int inlowmark, unsigned int inhightmark);

    serieDetect_c(unsigned int inlowmark = 0, unsigned int inhightmark = (SERIEDETECT_SIZE - 2))
    {
        setMark(inlowmark, inhightmark);
        cleanup();
    }
    ~serieDetect_c() {}

    void cleanup(void);

    bool addnCheck4True(bool newvalue); // return new state

    bool get_lastconfirmed(void) const;
    unsigned int getserieSize(void) const { return serieSize; }
    unsigned int getLowMark(void) const { return lowmark; }
    unsigned int getHiMark(void) const { return hightmark; }

private:
    static const unsigned int serieSize = SERIEDETECT_SIZE;
    static unsigned int lowmark;
    static unsigned int hightmark;

    bool _checkSerienUpdateInternalState(void);

    // priv fields
    unsigned int idx;
    bool arr[serieSize];
    bool lastconfirmed_val;
};

#endif